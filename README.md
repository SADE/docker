# Run exist-db as a service (SaaS) with 12-factor methodology in mind

Instructions and templates for an eXist-db docker setup.
This is intended to be used with docker and docker-compose or any other OCI compliant container runtimes.

## Build service(s)

```sh
./build
```

The build environment is configured with environment variables for which default values are provided in an `.env` file. It is a convention observed by `docker-compose` to read environment variables either from the (shell) environment or from that file.

If you like to add build arguments

- add key=value entry to `.env`
- add key=value entry to `docker-compose.yml` under the list of build args of the service that uses it during build
- add key=value entry to `Dockerfile` list of all available args in the beginning
- use it within the build process and access its value as a variable provided by the environment

The key names MUST be prefixed with `APP_` to prevent overwriting other environment variables.

## Start service(s)

```sh
./run
```

The app exposes its service at `http://localhost:8080`.

## Deploy a package of type app or resource

If the database deployment persists, before updating the package, the old version MUST be removed either manually or programmatically. (This feature is yet to be done, see [install/remove/upgrade app package with gitlab-ci](https://gitlab.gwdg.de/SADE/docker/-/issues/4))

## Executing commands with admin client

The eXist admin client is available in the container.
However it needs to be called by its classpath entry,
as neither the shell scripts nor a shell is included.
Some eXamples below:

### Java admin client

TBD: Run commands with `docker-compose run`, connect to the other database instance with xmlrpc (`xmldb:exist://localhost:8080/exist/xmlrpc`) to execute tasks

compare [Using the Command-line Client](http://exist-db.org/exist/apps/doc/java-admin-client#command-line)

create user:

```sh
docker-compose exec exist java -cp /exist/lib/exist.uber.jar org.exist.start.Main client --no-gui --xpath "sm:create-account('annotator', 'annotator', 'annotator')"
```

interactive shell:

```sh
docker-compose exec exist java -cp /exist/lib/exist.uber.jar org.exist.start.Main client --no-gui
```

### Backup / restore

compare [Restore using the Java Admin Client](http://exist-db.org/exist/apps/doc/backup#restore-jac)

create backup:

```sh
docker-compose exec exist java -cp /exist/lib/exist.uber.jar org.exist.backup.Main --backup /db -d /exist/backup/
```

restore backup:

```sh
docker-compose exec exist java -cp /exist/lib/exist.uber.jar org.exist.backup.Main --restore /[path-in-container-to]/__contents__.xml
```

Note: it may make sense to choose a backup path which is available outside the container, e.g. for Tivoli backups.
This can be configured in the volumes section of docker-compose.yml.

## Tested compatibility

Docker | Compose | Works
------ | ------- | -----
20.10  | 1.27    | (v)
19.03  | 1.25    | (v)
19.03  | 1.24    | (v)


=======

Adress Warnings:

```sh
[main] WARN  (Configuration.java [getConfigAttributeValue]:1528) - Configuration value overridden by system property: org.exist.db-connection.cacheSize, with value: 256M
[main] WARN  (Configuration.java [getConfigAttributeValue]:1528) - Configuration value overridden by system property: org.exist.db-connection.pool.max, with value: 20
...
[qtp1313618488-41] WARN  (Descriptor.java [<init>]:116) - Giving up unable to read descriptor.xml file from classloader in package org.exist.http, eXist-db Core, version 5.2
```

last one will be addressed with exist-db 5.2.1

backup gives the raw xml data from the database
