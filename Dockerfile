# list all available build args
ARG APP_ABBREV
ARG APP_AUTHOR
ARG APP_BUILD_DIR
ARG APP_DESC
ARG APP_LICENSE
ARG APP_NAME
ARG APP_TITLE
ARG APP_VERSION
ARG APP_WEBSITE


FROM existdb/existdb:5.2.0 AS exist-base
# base image of existdb is distroless; you can't `RUN mkdir ...`
WORKDIR /exist/backups
WORKDIR /exist
# overwrite default entrypoint to allow execution of arbitrary commands
ENTRYPOINT ["java"]
CMD ["org.exist.start.Main", "jetty"]


FROM debian:buster-slim AS app-builder
# list only used args
ARG APP_ABBREV
ARG APP_AUTHOR
ARG APP_BUILD_DIR
ARG APP_DESC
ARG APP_LICENSE
ARG APP_NAME
ARG APP_TITLE
ARG APP_VERSION
ARG APP_WEBSITE

ENV DEBIAN_FRONTEND=noninteractive

# avoid the problem of some packages that will not install if this directory does not exist
RUN mkdir /usr/share/man/man1/
RUN set -eux; \
    apt-get update; \
	apt-get install -y --no-install-recommends \
    ant
WORKDIR /${APP_ABBREV}
COPY ./${APP_ABBREV} .
# provide environment variables as a file to be used by ant both as a property file and as a filterset file
RUN env > .env
RUN ant


FROM exist-base AS exist-app
# list only used args
ARG APP_ABBREV
ARG APP_BUILD_DIR
ARG APP_VERSION

WORKDIR /exist/autodeploy
COPY --from=app-builder /${APP_ABBREV}/${APP_BUILD_DIR}/${APP_ABBREV}-${APP_VERSION}.xar .
WORKDIR /exist
